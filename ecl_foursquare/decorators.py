import json
import urllib

from signals import post_foursquare_auth
import constants

def foursquare_callback(fun):
    def k(request, *args, **kwargs):
        code = request.GET['code']

        params = constants.FOURSQUARE_AUTHENTICATION_PARAMS.copy()
        params['code'] = code
        params['client_secret'] = constants.FOURSQUARE_SECRET
        params['grant_type'] = 'authorization_code'
        del params['response_type']

        url = "https://foursquare.com/oauth2/access_token?" + urllib.urlencode(params)

        # Fetch the access token
        response = urllib.urlopen(url)
        data = json.load(response)

        token = data['access_token']

        post_foursquare_auth.send("ecl-foursquare", token=token, id=request.user.id)
        return fun(request, token, *args, **kwargs)
    return k
