import json
import urllib2
import urllib
from django.conf import settings
from ecl_tools.utils import Objectifier

FOURSQUARE_API_BASE = "https://api.foursquare.com/v2/"

class FoursquareError(Exception):
    def __init__(self, code, type, detail):
        self.code = str(code)
        self.type = type
        self.detail = detail

    def __str__(self):
        return "%s (%s), %s" % (self.code, self.type, self.detail)


class FoursquareCall(object):
    def __init__(self, token, endpoint_components, language='en'):
        self.token = token
        self.endpoint_components = endpoint_components
        self.language = language

    def __getattr__(self, k):
        self.endpoint_components.append(k)
        return FoursquareCall(self.token, self.endpoint_components, self.language)

    def __getitem__(self, k):
        self.endpoint_components.append(str(k))
        return FoursquareCall(self.token, self.endpoint_components, self.language)

    def __call__(self, method='GET', **kwargs):
        endpoint = "/".join(self.endpoint_components)
        headers = {'Accept-Language': self.language }
        kwargs['oauth_token'] = self.token
        kwargs['v'] = settings.FOURSQUARE_VERSION
        encoded_params = urllib.urlencode(kwargs)

        url = FOURSQUARE_API_BASE + endpoint
        if method == 'GET':
            url += "?" + encoded_params
            request = urllib2.Request(url, headers=headers)
        else:
            request = urllib2.Request(url, encoded_params, headers)

        try:
            response = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            data = json.load(e)
            code = data['meta']['code']
            type = data['meta']['errorType']
            detail = data['meta']['errorDetail']
            raise FoursquareError(code, type, detail)

        return Objectifier(json.load(response))


class Foursquare(object):
    def __init__(self, token, language='en'):
        """
        Example Usage

        >>> foursquare = Foursquare("3JUBENXURSR0RJNWOBQBTSNTBCQHQKOZW2USJYF25BXNXEMC")
        >>> foursquare.users[2603949]()
        >>> foursquare.users[2603949].checkins()
        """
        self.token = token
        self.language = language

    def __getattr__(self, k):
        return FoursquareCall(self.token, [k], self.language)

