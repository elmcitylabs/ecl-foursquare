from django.conf.urls.defaults import *

urlpatterns = patterns('ecl_foursquare.views',
    url(r'^begin$', 'foursquare_oauth_begin', name='foursquare-oauth-begin'),
    url(r'^complete$', 'foursquare_oauth_complete', name='foursquare-oauth-complete'),
)

