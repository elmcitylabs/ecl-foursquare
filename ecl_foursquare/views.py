from django.http import HttpResponseRedirect
from django.views.decorators.http import require_GET

from decorators import foursquare_callback
import constants

@require_GET
def foursquare_oauth_begin(request):
    return HttpResponseRedirect(constants.FOURSQUARE_AUTHENTICATION_URL)

@foursquare_callback
def foursquare_oauth_complete(request, token):
    return HttpResponseRedirect(constants.FOURSQUARE_POST_COMPLETE_URL)

