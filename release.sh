#!/bin/bash

git cim "push version to $1"
git archive --format=tar master > ecl_foursquare-$1.tar
gzip -f ecl_foursquare-$1.tar
s3cmd put ecl_foursquare-$1.tar.gz s3://packages.elmcitylabs.com/ -P
